# NOTED README #

As a non-technical user, I would like to be able to take quick notes in my browser. These notes would vary in length, from a few words up to whole paragraphs and should persist across page loads. The user must be able to delete these notes individually. You may use any framework or toolkit of your choice, but the application should load and render in a reasonable time (<3s) on a low-latency broadband connection. The application must support the latest version of the Chrome browser.

### NOTES ###

* Using Bower. Possibly switch to alternative due to deprecation
* Using LESS for speed due to recent SASS npm plugin issues

### TODO ###

* Namespace functions, tidy
* Date created to be dynamic (eg 7 mins ago; maybe only if in last 12 or 24 hours?)
* Don't empty archive on update, just add/change relevant items
* Remove jQuery
* Layouts (responsive)
* Warning if localstorage full (suggest delete some)
* Just one app div; others to be created dynamically
* Use Lockr to allow us to save more detailed objects to localstorage (instead of just strings)
* Truncate notes in archive (with 'show more')
* 'Saving' feedback icon?
* Loading existing note: just overwrite any in-progress notes? Save in-progress first? Dialogue?
* Only update archive once note has been submitted (not on type)?

### Features to be added ###

* Sorting
* Categorization/tagging
* Exporting data to a file
* Archiving
* Modification
