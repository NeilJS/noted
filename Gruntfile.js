'use strict';
module.exports = function(grunt) {
    // Load all tasks
    require('load-grunt-tasks')(grunt); // -- no need to manually load each grunt plugin, such as:    grunt.loadNpmTasks('grunt-xxx');
    // Show elapsed time
    require('time-grunt')(grunt);

    var jsFileList = [
        'assets/vendor/jquery/dist/jquery.min.js',
        'assets/vendor/readmore-js/readmore.js',
        'assets/js/main.js'
    ];

    grunt.initConfig({
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporterOutput: ''
            },
            all: [
                'Gruntfile.js',
                'assets/js/*.js',
                '!assets/js/scripts.js',
                '!assets/**/*.min.*'
            ]
        },
        less: {
            dev: {
                files: {
                    'css/style.css': [
                        'assets/less/style.less'
                    ]
                },
                options: {
                    compress: false,
                    sourceMap: true,
                    sourceMapFilename: 'css/style.css.map',
                    sourceMapURL: 'style.css.map'
                }
            },
            build: {
                files: {
                    'css/style.min.css': [
                        'assets/less/style.less'
                    ]
                },
                options: {
                    compress: true
                }
            }
        },
        uglify: {
            dev: {
                options: {
                    beautify: true,
                    mangle: false
                },
                files: {
                    'js/scripts.js': [jsFileList]
                }
            },
            build: {
                files: {
                    'js/scripts.min.js': [jsFileList]
                }
            }
        },
        watch: {
            css: {
                files: [
                    'assets/less/*.less',
                    'assets/less/**/*.less'
                ],
                tasks: ['less:dev']
            },
            js: {
                files: [
                    jsFileList,
                    '<%= jshint.all %>'
                ],
                tasks: ['jshint', 'uglify:dev']
            }
        },
        concurrent: {
            options: {
                limit: 4,
                logConcurrentOutput: true
            },
            dev:['less:dev', 'jshint', 'uglify:dev'],
            build:['less:build', 'jshint', 'uglify:build']
        }
    });

    grunt.registerTask('dev', ['concurrent:dev', 'watch']);
    grunt.registerTask('build', ['concurrent:build', 'watch']);
};
