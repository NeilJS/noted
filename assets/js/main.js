console.log("Let's begin...");


$(document).ready(function() {

    // Elements
    var $textarea = $(".noted_form-textarea");
    var $archive = $(".noted_archive");

    var note_content = "";
    var recently_loaded = null;



    // Check localstorage support
    var has_localstorage = (typeof(Storage) !== "undefined");



    // Prepare textarea
    function prepare_textarea() {
        note_content = "";
        var timestamp = Date.now();
        //console.log("timestamp: " + timestamp);
        //alert(new Date(timestamp).toGMTString());
        $textarea.val("");
        $textarea.attr("data-timestamp", timestamp); // attr so we can see in devtools
        $textarea.focus();
    }
    prepare_textarea();



    // Get Notes when user finished typing
    var user_typed_timeout = null;
    $textarea.keyup(function() {
        clearTimeout(user_typed_timeout);
        user_typed_timeout = setTimeout(function() {
            note_content = $textarea.val();
            console.log('User typed: "' + note_content + '"');
            save_note(note_content);
            show_notes(); // update list in real-time?
        }, 500);
    });



    // Save current note to localstorage
    function save_note(note) {
        var timestamp = $textarea.attr("data-timestamp"); // .attr() updating better than .data()
        //alert(timestamp);
        if (has_localstorage) {
            console.log("Saving "+timestamp+": \n\n" + note);
            localStorage[timestamp] = note;
        } else {
            alert("Your browser does not support saving data");
        }

    }



    // New note: save current and reset textarea
    $(".noted_controls-new_note").on("click", function(e){
        e.preventDefault();
        if (note_content) {
            console.log("Save current note.");
            save_note(note_content);
            console.log("Prepare textarea.");
            prepare_textarea();
            show_notes();
        } else {
            console.log("No content to save.");
        }
    });



    // List existing notes (Title/ truncated into + pretty timestamp)
    function show_notes() {
        console.log("\nSaved notes...\n");
        $archive.empty();
        $.each(localStorage, function(key, value){
            if (typeof value === "string") {
                console.log(key + ": " + value);
                // Get human-readable date/time note was created
                var timestamp = parseInt(key);
                var note_date = new Date(timestamp).toGMTString();
                // Build the <li> item
                var $archive_item = $("<li class=\"noted_archive-item\" data-timestamp=\""+timestamp+"\"></li>");
                // Insert the note content
                $archive_item.html("<div class='noted_archive-content'>" + value + "</div>" + "<time>" + note_date + "</time>");
                $archive.prepend($archive_item);
            }
        });

        // Readmore.js - see: https://github.com/jedfoster/Readmore.js
        $archive.find("li .noted_archive-content").readmore({
            speed: 75,
            //collapsedHeight: "80px",
            lessLink: '<a href="#">Read less</a>'
        });

    }
    show_notes();



    // Load existing note
    function load_note(key) {
        if (key === 0) {
            return false;
        }
        var timestamp = key;
        note_content = localStorage[key];
        console.log("Ready to load note " + timestamp + ": \n" + note_content);
        //console.log(localStorage[key]);
        $textarea.val(note_content);
        $textarea.attr("data-timestamp", timestamp);
        // Remember most recently loaded note
        //localStorage.setItem("recent_loaded", timestamp); // todo
    }



    // Load latest note by default (if no recently loaded - todo)
    function get_latest_note() {
        var largest = 0;
        for (var key in localStorage){
           console.log("-- " + key);
           if (key > largest) {
                largest = key;
           }
        }
        var latest_key = largest;
        console.log("Auto load latest: " + latest_key);
        return latest_key;
    }
    load_note(get_latest_note());




    // Listen for clicks on archived notes (so we can load them)
    $archive.on("click", "li", function() {
        //var index = $(this).index(); // use key instead.
        var key = $(this).data("timestamp");
        load_note(key);
        // Set active state on archive item
        $(this).addClass("noted_archive-item--active").siblings().removeClass("noted_archive-item--active");
    });



    // Trash the current/loaded note
    $(".noted_controls-delete_note").on("click", function(e){
        e.preventDefault();
        if ($textarea.val()) {
            var timestamp = $textarea.attr("data-timestamp");
            console.log("Trash current note: " + timestamp);
            localStorage.removeItem(timestamp);
            prepare_textarea();
            // Also remove item from archive
            var $remove_li = $archive.find("li[data-timestamp="+timestamp+"]");
            //console.log($remove_li);
            $remove_li.fadeOut("200", function() {
                $(this).remove();
            });
        } else {
            console.log("No content to trash.");
        }
    });





});